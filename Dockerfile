FROM ubuntu:16.04
RUN apt-get update && apt-get -y install git apache2 openssh-server bash
RUN cd /var/www && git clone http://bitbucket.org/jfbethlehem/dxctf_challenge2 html
RUN rm -rf /var/www/html/.git
RUN useradd admin -p secret -d /home/admin -G sudo -s /bin/bash -U 
RUN mv /var/www/html/secret/flag.txt /root/
RUN chown root:root /root/flag.txt
RUN chmod 600 /root/flag.txt
RUN /etc/init.d/apache2 restart
RUN /etc/init.d/ssh restart
EXPOSE 80
EXPOSE 22
